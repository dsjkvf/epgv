epgv.py
=======

## About

`epgv` is a simple Python script to print the contents of a remote [Electronic Program Guide (EPG)](https://en.wikipedia.org/wiki/Electronic_program_guide) file to the standard output in a human readable form:

![sample](https://i.imgur.com/aOuJZwS.png)

## Dependencies

`epgv` uses `gawk`, `awk`, `gsed`, or `sed` to speed up some routines (`--version` will report which one exactly is in use, and if none are available, the program will just exit).

## Usage

The following command line arguments are supported:

  - to provide the URL of the remote EPG file: `--url`;
  - to provide the path of the local EPG file: `--file` (one of these two is required);
  - to list the available channels from the EPG file: `--channels`;
  - to limit the number of the forthcoming programs to print out: `--limit`;
  - to show only preferred channels in a preferred order: `--preferred`;
  - to print the channels in the schedule in a reversed order: `--reversed`;
  - to display version and some details: `--version`;
  - to show this help: `--help`.

### Helpers

`epgf` is a helper script that can 'flatten' a provided EPG file according to a supplied filter (i.e., it will delete all the channels except for those that are mentioned in a filter file) in order to print out the schedules way, way faster.

### Examples

Like this:

    # to display the contents of an EPG file:
    epgv.py --url=https://SITE.DOMAIN/YOUR/EPG_FILE.xml.gz
    # to display the contents of an EPG file and limit the number of the fortcoming programs to 3:
    epgv.py --url=https://SITE.DOMAIN/YOUR/EPG_FILE.xml.gz --limit=3
    # to display the contents of an EPG file, while limiting the output only to chanels 2 and 5
    epgv.py --url=https://SITE.DOMAIN/YOUR/EPG_FILE.xml.gz --preferred='2, 5'
    # to display the contents of an EPG file, while limiting the number of forthcoming programs
    # to 2 (only the currently aired and the next one) and also dislaying those only for channel 1
    epgv.py --url=https://SITE.DOMAIN/YOUR/EPG_FILE.xml.gz --limit=2 --preferred=1
    # 'flatten' an existing EPG (will produce epg_flt.xml.gz)
    epgf.py epg.xml filter.txt

## Extras

Single binary builds are available for [Windows](https://gitlab.com/dsjkvf/epgv-builds/-/jobs/artifacts/master/download?job=build-windows) and [Linux](https://gitlab.com/dsjkvf/epgv-builds/-/jobs/artifacts/master/download?job=build-linux) platforms.



