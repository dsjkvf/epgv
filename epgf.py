#!/usr/bin/env python3
import sys
import os
import xml.etree.ElementTree as ET
import gzip

def help():
    print("USAGE: ")
    print("      epgf <EPG-FILE> <FILTER-FILE>")
    sys.exit()

def getelements(file, filter):
    # init filter
    try:
        with open(filter) as f:
            c = f.read().splitlines()
    except:
        print("ERROR: Can't read the filter file")
        sys.exit()

    # iterate file according to the above filter
    context = iter(ET.iterparse(file, events=('start', 'end')))
    _, root = next(context)
    for event, elem in context:
        if event == 'end' and elem.tag == 'channel':
            if elem.attrib['id'] in c:
                yield ET.tostring(elem, encoding='utf-8')
        elif event == 'end' and elem.tag == 'programme':
            if elem.attrib['channel'] in c:
                yield ET.tostring(elem, encoding='utf-8')
        root.clear()

try:
    epg_file = sys.argv[1]
    epg_filter = sys.argv[2]
except:
    epg_file = 'epg.xml'
    epg_filter = 'epg_flt.txt'
if not os.access(epg_file, os.R_OK) or not os.access(epg_filter, os.R_OK):
    help()

with gzip.open('epg_flt.xml.gz', 'wb') as f:
    # start root
    f.write(b'<!DOCTYPE tv SYSTEM "https://iptvx.one/xmltv.dtd">\n<tv generator-info-name="IptvX.one" generator-info-url="https://iptvx.one/">\n')
    # add element
    for element in getelements(epg_file, epg_filter):
        if element:
            f.write(element)
    # close root
    f.write(b'\n</tv>')
