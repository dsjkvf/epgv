#!/usr/bin/env python3


# MODULES

# System

# fix the path
import site
site.addsitedir('/home/s/.local/lib/python3.6/site-packages')

# fix the encoding
import sys
import codecs
if sys.stdout.encoding.lower() != 'utf-8':
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout.detach())

# load the rest
import cgi
import gzip
import time
from datetime import datetime, timedelta
from pytz import timezone
from bs4 import BeautifulSoup


# GLOBALS

# Preferences
# set the timezone
TMZ='EET'
LIMIT = 44
FILE = 'epg_flt.xml'
FILTER = 'epg_flt.txt'


# MAIN

# Check the preferences
try:
    if cgi.FieldStorage().getlist('d')[0] == '2':
        LIMIT = 88
except:
    pass
# Get the channels
the_cont = ''
with open(FILE, 'r', encoding="utf-8") as f:
    for line in f:
        if line.startswith('<channel'):
            the_cont += line
        elif line.startswith('<programme'):
            break
soup = BeautifulSoup(the_cont, "lxml")
chan = {}
for i in soup.find_all('channel'):
    chan[i.attrs['id']] = i.findAll('display-name')[-1].text

# Fix the channels names
for k, v in chan.items():
    if ' HD' in v:
        chan[k] = chan[k].replace(' HD', '')
chan['amedia-premium'] = 'Amedia Premium'
chan['amedia-hit'] = 'Amedia Hit'
chan['amedia1'] = 'Amedia 1'
chan['amedia2'] = 'Amedia 2'
chan['hollywood-hd'] = 'Hollywood HD'
chan['prikluchenia-hd'] = 'Приключения'
chan['teleputeshestvia'] = 'Путешествия'
chan['moya-planeta'] = 'Моя планета'

# Sort the channels
# read the filter
with open(FILTER, 'r', encoding="utf-8") as f:
    chan_order = f.read().splitlines()
# create a map according to the above filter
chan_map = {v: i for i, v in enumerate(chan_order)}
# create a sorted list containing key/value pairs according to the above map
chan_sorted_list = sorted(chan.items(), key=lambda pair: chan_map[pair[0]])
# create a dict according to the above sorted list
chan=dict([(v,k) for v,k in chan_sorted_list])

# Get the programs
the_cont = ''
with open(FILE, 'r', encoding="utf-8") as f:
    the_cont = f.read()
soup = BeautifulSoup(the_cont, "lxml")

prog = {}
for i in chan.keys():
    prog['p' + i] = []

if sys.version_info < (3,6):
    # versions prior to 3.6 throw ValueError: astimezone() cannot be applied to a naive datetime
    now = timezone(TMZ).localize(datetime.now())
else:
    now = datetime.now().astimezone(timezone(TMZ))
for i in soup.find_all('programme'):
    the_time = datetime.strptime(i.attrs['start'], '%Y%m%d%H%M%S %z').astimezone(timezone(TMZ))
    if the_time >= now:
        if LIMIT and len(prog['p' + i.attrs['channel']]) > LIMIT - 1:
            continue
        else:
            prog['p' + i.attrs['channel']].append(the_time.strftime('%Y-%m-%d %H:%M') + ' | ' + i.findAll('title')[0].text)
    elif the_time + timedelta(minutes=120) >= now:
        if len(prog['p' + i.attrs['channel']]) > 0:
            prog['p' + i.attrs['channel']].pop()
        prog['p' + i.attrs['channel']].append(the_time.strftime('%Y-%m-%d') + ' -NOW- | ' + i.findAll('title')[0].text)

# Print the results
print('Content-type: text/html; charset=utf-8\n\n\n')
print('<head>')
print('<title>')
print('TV/EPG')
print('</title>')
print('</head>')
print('<body>')
print('<pre>')
print('<table>')
for i in chan.keys():
    try:
        print('<tr><td>&nbsp;</td></tr><tr><th colspan="3" style="text-align:left">=== ' + chan[i] + ' ===</th></tr>')
    except KeyError:
        print("ERROR: Wrong preferred channels provided (use '--channels' for the reference)")
        raise SystemExit
    for j in prog['p' + i]:
        if LIMIT == 44:
            if sys.version_info < (3,6):
                # versions prior to 3.6 throw ValueError: astimezone() cannot be applied to a naive datetime
                tomorrow = (timezone(TMZ).localize(datetime.now()) + timedelta(1)).strftime('%Y-%m-%d')
            else:
                tomorrow = (datetime.now().astimezone(timezone(TMZ)) + timedelta(1)).strftime('%Y-%m-%d')
            if j.split("|")[0][:10] >= tomorrow:
                continue
        if LIMIT == 88:
            if sys.version_info < (3,6):
                # versions prior to 3.6 throw ValueError: astimezone() cannot be applied to a naive datetime
                tomorrow = (timezone(TMZ).localize(datetime.now()) + timedelta(1)).strftime('%Y-%m-%d')
            else:
                tomorrow = (datetime.now().astimezone(timezone(TMZ)) + timedelta(1)).strftime('%Y-%m-%d')
            if j.split("|")[0][:10] != tomorrow:
                continue
        print('<tr><td><span style="color:#c0c0c0">' + j.split("|")[0] + '</span></td><td>&nbsp;&nbsp;&nbsp;</td>', end='')
        if 'LIVE!' in j:
            print('<td><span style="color:#bea600">' + j.split("|")[1][:100] + '</span></td></tr>')
        else:
            print('<td>' + j.split("|")[1][:100] + '</td></tr>')
print('</table>')
print('</pre>')
print('</body>')
