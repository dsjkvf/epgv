#!/usr/bin/env python3
# -*- coding: utf-8 -*-


# HEADER

# Details
__title__      = "The Electronic Program Guide viewer"
__author__     = "dsjkvf"
__maintainer__ = "dsjkvf"
__email__      = "dsjkvf@gmail.com"
__copyright__  = "dsjkvf"
__credits__    = []
__license__    = "GPL"
__version__    = "1.0.6"
__status__     = "Production"


# MODULES

# System
import sys
import shutil
import argparse
import tempfile
import urllib.request
import ssl
import subprocess
import os
import time
from datetime import datetime, timedelta
from pytz import timezone
from bs4 import BeautifulSoup

# Extrenal
# set the possible external tools
bins = [['gawk', ''], ['gsed -n -E', 'p'], ['awk', ''], ['sed -n -E', 'p']]
# check for sed's version
try:
    if not subprocess.call(['sed', '--version'], stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL):
        # exits with 0, meaning GNU sed is installed as sed, don't check for gsed
        bins = [['gawk', ''], ['sed -n -E', 'p'], ['awk', '']]
# seems like no sed present (gsed can still be there, though)
except:
    pass
bin = [i for i in bins if shutil.which(i[0].split()[0])][0]
if not bin:
    print('ERROR: Neither gawk, awk or sed were found; exiting')
    raise SystemExit


# GLOBALS

# Preferences
# set the timezone
TMZ = 'EET'


# MAIN

# Parse the command line arguments
parser = argparse.ArgumentParser(description='A viewer for Electronic Program Guide files', usage='%(prog)s [options]')
parser.add_argument("-v", "--version", help="show program version", action="store_true")
parser.add_argument("-u", "--url", type=str, help="set the location of the remote EPG.xml.gz")
parser.add_argument("-f", "--file", type=str, help="set the location of the local EPG.xml.gz")
parser.add_argument("-c", "--channels", help="show only the channels", action="store_true")
parser.add_argument("-l", "--limit", default=0, type=int, metavar='N', help="set the number of entries to display")
parser.add_argument("-p", "--preferred", default='', type=str, metavar="'1, 2, 3'", help="set the preferred order of channels")
parser.add_argument("-r", "--reversed", help='reverse the print order', action='store_true')
args = parser.parse_args()
if args.version:
    print(__title__ + " v." + __version__ + " [by " + __author__ + " <" + __email__ + ">]")
    print("(using " + bin[0].split()[0] + " for internal routines)")
    raise SystemExit
if not args.url and not args.file:
    parser.print_help()
    print('\nERROR: Either --url or --file should be used')
    raise SystemExit
the_remo = args.url
the_loca = args.file
the_chan = args.channels
the_limi = args.limit
the_pref = args.preferred
if the_pref[-1] == ',': the_pref = the_pref[:-1]
the_rev = args.reversed

# Init the temp files
tempdir = tempfile.TemporaryDirectory()
temppth = tempdir.name
tempxgz = temppth + '/z.xml.gz'
tempxml = temppth + '/z.xml'
tempchn = temppth + '/z.chn'
tempprg = temppth + '/z.prg'

# Get the data
# if both --url and --file options are present, compare their timestamps
ssl._create_default_https_context = ssl._create_unverified_context
if the_loca and the_remo:
    try:
        # get the "Last Modified" time of the remote file
        remconn = urllib.request.urlopen(the_remo, timeout=30)
        # convert that time into a timestamp
        remdate = time.mktime(datetime.strptime(remconn.headers['last-modified'], "%a, %d %b %Y %X GMT").timetuple())
    # stop the script in case of error
    except urllib.error.HTTPError as e:
        print("HTTP Error:", e.code, the_remo)
        print("Trying to use the local version")
        remdate = 0
    except urllib.error.URLError as e:
        print("URL Error:", e.reason, the_remo)
        print("Trying to use the local version")
        remdate = 0
    # get the "Last Modified" timestamp of the local file
    try:
        locdate = time.mktime(datetime.utcfromtimestamp(os.path.getmtime(the_loca)).timetuple())
    except OSError:
        locdate = 0
    # compare those and act accordingly
    # if the local EPG is older, update it and then use it
    if locdate < remdate:
        print("Updating...")
        urllib.request.urlretrieve(the_remo, tempxgz)
        subprocess.call(['gzip -d ' + tempxgz], shell=True)
        subprocess.call(['cp', tempxml, the_loca])
    # if the local EPG is younger, just use it
    elif locdate > remdate:
        subprocess.call(['cp', the_loca, tempxml])
    # if both are unavailable, report and exit
    elif remdate == 0 and locdate == 0:
        print("ERROR: Both the remote and the local lists are unavailable")
        raise SystemExit
# if only --url option provided, use the remote EPG
elif the_remo:
    urllib.request.urlretrieve(the_remo, tempxgz)
    subprocess.call(['gzip -d ' + tempxgz], shell=True)
# if only --file option provided, use the local EPG
elif the_loca:
    if the_loca[-3:] == '.gz':
        subprocess.call(['gzip -d --to-stdout ' + the_loca + ' > ' + tempxml], shell=True)
    else:
        subprocess.call(['cp', the_loca, tempxml])

# Get the channels
subprocess.call([bin[0] + " '/channel id=/,/<\/channel>/" + bin[1] + "' " + tempxml + " > " + tempchn], shell=True)
with open(tempchn, 'r') as tmp_file:
    the_cont = tmp_file.read()
soup = BeautifulSoup(the_cont, features="lxml")
chan = {}
for i in soup.find_all('channel'):
    chan[i.attrs['id']] = i.findAll('display-name')[-1].text

# print the channels if requested
if the_chan:
    def my_key(dict_key):
        try:
            return int(dict_key)
        except ValueError:
            return dict_key
    for i in sorted(chan, key=my_key):
        print(i + ": " + chan[i])
    raise SystemExit

# Get the programs
if the_pref:
    subprocess.call([bin[0] + " '/channel=\"" + the_pref.replace(', ', ',').replace(',', '"|channel=\"') + "\"/,/<\\/programme>/" + bin[1] + "' " + tempxml + " > " + tempprg], shell=True)
    pref = the_pref.replace(', ', ',').split(',')
    with open(tempprg, 'r') as tmp_file:
        the_cont = tmp_file.read()
else:
    pref = chan.keys()
    with open(tempxml, 'r') as tmp_file:
        the_cont = tmp_file.read()
soup = BeautifulSoup(the_cont, features="lxml")

prog = {}
for i in chan.keys():
    prog['p' + i] = []

if sys.version_info < (3, 6):
    # versions prior to 3.6 throw ValueError: astimezone() cannot be applied to a naive datetime
    now = timezone(TMZ).localize(datetime.now())
else:
    now = datetime.now().astimezone(timezone(TMZ))
for i in soup.find_all('programme'):
    the_time = datetime.strptime(i.attrs['start'], '%Y%m%d%H%M%S %z').astimezone(timezone(TMZ))
    if the_time >= now:
        if the_limi and len(prog['p' + i.attrs['channel']]) > the_limi - 1:
            continue
        else:
            prog['p' + i.attrs['channel']].append(the_time.strftime('%Y-%m-%d %H:%M') + ' | ' + i.findAll('title')[0].text)
    elif the_time + timedelta(minutes=120) >= now:
        if len(prog['p' + i.attrs['channel']]) > 0:
            prog['p' + i.attrs['channel']].pop()
        prog['p' + i.attrs['channel']].append(the_time.strftime('%Y-%m-%d') + ' -NOW- | ' + i.findAll('title')[0].text)

# Print the result
if the_rev:
    pref = reversed(pref)
for i in pref:
    try:
        print('\n=== ' + chan[i] + ' ===')
    except KeyError:
        print("ERROR: Wrong preferred channels provided (use '--channels' for the reference)")
        raise SystemExit
    for j in prog['p' + i]:
        if the_limi == 44:
            if sys.version_info < (3, 6):
                # versions prior to 3.6 throw ValueError: astimezone() cannot be applied to a naive datetime
                tomorrow = (timezone(TMZ).localize(datetime.now()) + timedelta(1)).strftime('%Y-%m-%d')
            else:
                tomorrow = (datetime.now().astimezone(timezone(TMZ)) + timedelta(1)).strftime('%Y-%m-%d')
            if j.split("|")[0][:10] >= tomorrow:
                continue
        if the_limi == 88:
            if sys.version_info < (3, 6):
                # versions prior to 3.6 throw ValueError: astimezone() cannot be applied to a naive datetime
                tomorrow = (timezone(TMZ).localize(datetime.now()) + timedelta(1)).strftime('%Y-%m-%d')
            else:
                tomorrow = (datetime.now().astimezone(timezone(TMZ)) + timedelta(1)).strftime('%Y-%m-%d')
            if j.split("|")[0][:10] != tomorrow:
                continue
        sys.stdout.write("\033[0;90m")
        print(j.split("|")[0] + '|', end='')
        sys.stdout.write("\033[0;0m")
        if 'LIVE!' in j.split("|")[1] or 'Tiesiogiai' in j.split("|")[1]:
            sys.stdout.write("\033[0;33m")
            print(j.split("|")[1])
            sys.stdout.write("\033[0;0m")
        else:
            print(j.split("|")[1])

# Clean up
tempdir.cleanup()
