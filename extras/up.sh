#!/bin/bash

# cwd && download && flatten && gunzip + keep gzipped
cd $(dirname $0) && curl -sL https://iptvx.one/EPG_LITE | gzip -d - > epg.xml && ./epgf.py && rm epg.xml && gunzip -c epg_flt.xml.gz > epg_flt.xml
